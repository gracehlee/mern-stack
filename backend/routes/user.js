const express = require("express");

// controller functions
const router = express.Router();

const { loginUser, signupUser } = require("../controllers/userController");

// login route
router.post("/login", loginUser);

// signup route
router.post("/signup", signupUser);

module.exports = router;
