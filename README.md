# MERN stack

## Intro

Practice project to practice MERN project stack.
MongoDB - ExpressJS - React - NodeJS

## Set Up

In path mern-stack/backend, you will need to create a .env file with the variables:
```javascript
PORT=4000
MONG_URI="mongodb+srv://MongoDBUsername:MongoDBPassword@AppName.f4prksp.mongodb.net/"
SECRET="<secret string here for jwt token>"
```

The MONG_URI is the URI associated with the MongoDB database set to your project. The database is set to private for this repository, so anyone downloading these project files will need to set up their own using MongoDB Atlas: https://www.mongodb.com/products/platform/atlas-database
