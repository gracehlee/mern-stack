import { useState } from "react";
import { useWorkoutContext } from "../hooks/useWorkoutContext";

const WorkoutForm = () => {
  const { dispatch } = useWorkoutContext();
  const [formData, setFormData] = useState({
    title: "",
    load: "",
    reps: "",
  });

  const [alert, setAlert] = useState(null);
  const [error, setError] = useState(null);
  const [emptyFields, setEmptyFields] = useState([]);

  const handleFormInput = (event) => {
    setFormData({
      ...formData,
      [event.target.name]: event.target.value,
    });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    const workout = formData;

    const response = await fetch("/api/workouts", {
      method: "POST",
      body: JSON.stringify(workout),
      headers: {
        "Content-Type": "application/json",
      },
    });

    const json_workout = await response.json();

    if (!response.ok) {
      setError(json_workout.error);
      setAlert(null);
      setEmptyFields(json_workout.emptyFields);
      console.log(error);
      if (emptyFields.length > 0) {
        setError("Please fill in all of the fields.");
      } else {
        setError("Unable to create new workout.");
      }
    }

    if (response.ok) {
      setFormData({
        title: "",
        load: "",
        reps: "",
      });
      setError(null);
      setAlert("New workout created");
      setEmptyFields([]);
      console.log("New workout created", json_workout);
      dispatch({ type: "CREATE_WORKOUT", payload: json_workout });
    }
  };

  return (
    <form className="create" onSubmit={handleSubmit}>
      <h3>Add a New Workout</h3>

      <label>Exercise Title:</label>
      <input
        type="text"
        onChange={handleFormInput}
        name="title"
        value={formData.title}
        className={emptyFields.includes("title") ? "error" : ""}
      />
      <label>Load (kg):</label>
      <input
        type="number"
        onChange={handleFormInput}
        name="load"
        value={formData.load}
        className={emptyFields.includes("load") ? "error" : ""}
      />
      <label>Reps:</label>
      <input
        type="number"
        onChange={handleFormInput}
        name="reps"
        value={formData.reps}
        className={emptyFields.includes("reps") ? "error" : ""}
      />
      <button>Submit</button>
      {alert && <div className="alert">{alert}</div>}
      {error && <div className="error">{error}</div>}
    </form>
  );
};

export default WorkoutForm;
