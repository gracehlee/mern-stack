import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <header>
      <div className="container">
        <Link to="/">
          <h1>Workout Buddy</h1>
        </Link>
        <nav>
          <div>
            <Link style={{ marginRight: "20px" }} to="/login">
              Login
            </Link>
            <Link style={{ marginRight: "20px" }} to="/signup">
              Signup
            </Link>
          </div>
        </nav>
      </div>
    </header>
  );
};

export default Navbar;
